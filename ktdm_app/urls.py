from django.conf.urls import include, url
from . import views
from django.http import HttpResponse, HttpResponseNotFound, Http404,  HttpResponseRedirect


urlpatterns = [
    url(r'^category$', views.category_list, name='category_list'),
    url(r'^category/(?P<pk>\d+)/$', views.category_detail, name='category_detail'),
    url(r'^$', views.post_list, name='post_list'),
    url(r'^post/(?P<pk>\d+)/$', views.post_detail, name='post_detail'),

    # url(r'^blog/$', views.redir_to_main, name='redir_to_main')
]
# handler404 = 'ktdm_app.views.view_404'