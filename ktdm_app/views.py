from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from ktdm_app.models import Post, Category
from django.http import HttpResponse, HttpResponseNotFound, Http404,  HttpResponseRedirect
from constance import config
from django.shortcuts import redirect
# Create your views here.



def post_list(request):
    posts = Post.objects.filter(published_at__lte=timezone.now()).order_by('-published_at')

    return render(request, 'blog/base_post_list.html', {'posts': posts})


def post_detail(request, pk):

    if config.SHOW_TIMER != True : 
       post = get_object_or_404(Post, published_at__lte=timezone.now(), pk=pk)
    else :
       post = get_object_or_404(Post, pk=pk)

    
    return render(request, 'blog/base_post_detail.html', {'post': post})


def category_list(request):
    # this will get all categories, you can do some filtering if you need (e.g. excluding categories without posts in it)
    categories = Category.objects.all()

    # blog/category_list.html should be the template that categories are listed.
    return render(request, 'blog/base_category_list.html', {'categories': categories})


def category_detail(request, pk):
    category = get_object_or_404(Category, pk=pk)
    posts = Post.objects.filter(category=pk).filter(published_at__lte=timezone.now()).order_by('-published_at')
    # in this template, you will have access to category and posts under that category by (category.post_set).
    return render(request, 'blog/base_category_post_list.html', {'category': category, 'posts' : posts})
