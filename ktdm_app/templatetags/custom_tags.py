from django import template
register = template.Library()

from ..models import Category

@register.simple_tag(name='cat_nav')
def category_nav():
    return Category.objects.all()